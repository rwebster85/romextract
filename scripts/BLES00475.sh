#!/bin/sh
	
# Extraction script for:
# flog_u.fpg from encrypted and extracted Sega Mega Drive Ultimate Collection (Europe).iso

# Outputs:
# - Alex Kidd in the Enchanted Castle (USA).md
# - Alien Storm (World).md
# - Altered Beast (USA, Europe).md
# - Bare Knuckle - Ikari no Tekken ~ Streets of Rage (World) (Rev A).md
# - Beyond Oasis (USA).md
# - Bonanza Bros. (Japan, Europe) (Rev A).md
# - Columns (USA, Europe).md
# - Comix Zone (USA).md
# - DEcapAttack (USA, Europe, Korea).md
# - Dr. Robotniks Mean Bean Machine (USA).md
# - Dynamite Headdy (USA, Europe).md
# - Ecco the Dolphin (USA, Europe, Korea).md
# - Ecco - The Tides of Time (USA).md
# - ESWAT - City under Siege (USA, Europe) (Rev A).md
# - Fatal Labyrinth (USA, Europe).md
# - Flicky (USA, Europe).md
# - Gain Ground (USA).md
# - Golden Axe (World) (Rev A).md
# - Golden Axe II (World).md
# - Golden Axe III (Japan) (En).md
# - Kid Chameleon (USA, Europe).md
# - Phantasy Star II (USA, Europe) (Rev A).md
# - Phantasy Star III - Generations of Doom (USA, Europe, Korea).md
# - Phantasy Star IV (USA).md
# - Ristar (USA, Europe).md
# - Shining Force (USA).md
# - Shining Force II (USA).md
# - Shining in the Darkness (USA, Europe).md
# - Shinobi III - Return of the Ninja Master (USA).md
# - Sonic 3D Blast ~ Sonic 3D Flickies Island (USA, Europe, Korea).md
# - Sonic & Knuckles (World).md
# - Sonic Spinball (USA).md
# - Sonic The Hedgehog (Japan, Korea).md
# - Sonic The Hedgehog 2 (World) (Rev A).md
# - Sonic The Hedgehog 3 (USA).md
# - Streets of Rage 2 (USA).md
# - Streets of Rage 3 (USA).md
# - Super Thunder Blade (World).md
# - Vectorman (USA, Europe).md
# - Vectorman 2 (USA).md
# - Golden Axe Warrior (USA, Europe, Brazil).sms
# - Phantasy Star (USA, Europe) (Rev 1).sms

# Requires: -

romextract()
{
	# TODO: Extract PS3 ISO
	# If possible add suppport for arcade games:
	# - Alien Syndrome
	# - Altered Beast
	# - Congo Bongo/Tip Top
	# - Fantasy Zone
	# - Shinobi
	# - Space Harrier
	# - Zaxxon

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID" || return 1

	echo "Extracting files from flog_u.fpg ..."

	tail -c +794625 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Alien Storm (World).md.zlib"
	tail -c +1605633 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Bonanza Bros. (Japan, Europe) (Rev A).md.zlib"
	tail -c +1804289 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Columns (USA, Europe).md.zlib"
	tail -c +2252801 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Fatal Labyrinth (USA, Europe).md.zlib"
	tail -c +2357249 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Flicky (USA, Europe).md.zlib"
	tail -c +2402305 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Golden Axe II (World).md.zlib"
	tail -c +2738177 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Golden Axe III (Japan) (En).md.zlib"
	tail -c +3424257 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Golden Axe Warrior (USA, Europe, Brazil).sms.zlib"
	tail -c +3569665 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Golden Axe (World) (Rev A).md.zlib"
	tail -c +3919873 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Kid Chameleon (USA, Europe).md.zlib"
	tail -c +5115905 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Sonic The Hedgehog 2 (World) (Rev A).md.zlib"
	tail -c +5867521 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Sonic 3D Blast ~ Sonic 3D Flickies' Island (USA, Europe, Korea).md.zlib"
	tail -c +8488961 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Sonic & Knuckles (World).md.zlib"
	tail -c +9779201 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Sonic The Hedgehog (Japan, Korea).md.zlib"
	tail -c +10868737 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Super Thunder Blade (World).md.zlib"
	tail -c +11126785 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Bare Knuckle - Ikari no Tekken ~ Streets of Rage (World) (Rev A).md.zlib"
	tail -c +11499521 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Vectorman 2 (USA).md.zlib"
	tail -c +12875777 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Vectorman (USA, Europe).md.zlib"
	tail -c +18245633 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Alex Kidd in the Enchanted Castle (USA).md.zlib"
	tail -c +18399233 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Altered Beast (USA, Europe).md.zlib"
	tail -c +18724865 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Beyond Oasis (USA).md.zlib"
	tail -c +20535297 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Comix Zone (USA).md.zlib"
	tail -c +22157313 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/DEcapAttack (USA, Europe, Korea).md.zlib"
	tail -c +22411265 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Dynamite Headdy (USA, Europe).md.zlib"
	tail -c +23810049 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Ecco - The Tides of Time (USA).md.zlib"
	tail -c +25042945 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Ecco the Dolphin (USA, Europe, Korea).md.zlib"
	tail -c +25561089 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/ESWAT - City under Siege (USA, Europe) (Rev A).md.zlib"
	tail -c +25937921 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Gain Ground (USA).md.zlib"
	tail -c +26304513 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Phantasy Star II (USA, Europe) (Rev A).md.zlib"
	tail -c +26779649 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Phantasy Star III - Generations of Doom (USA, Europe, Korea).md.zlib"
	tail -c +27232257 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Phantasy Star IV (USA).md.zlib"
	tail -c +29624321 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Phantasy Star (USA, Europe) (Rev 1).sms.zlib"
	tail -c +29943809 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Ristar (USA, Europe).md.zlib"
	tail -c +31358977 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Dr. Robotnik's Mean Bean Machine (USA).md.zlib"
	tail -c +31930369 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Shining Force II (USA).md.zlib"
	tail -c +33437697 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Shining in the Darkness (USA, Europe).md.zlib"
	tail -c +34068481 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Shining Force (USA).md.zlib"
	tail -c +35244033 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Shinobi III - Return of the Ninja Master (USA).md.zlib"
	tail -c +35883009 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Sonic The Hedgehog 3 (USA).md.zlib"
	tail -c +37142529 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Sonic Spinball (USA).md.zlib"
	tail -c +37670913 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Streets of Rage 2 (USA).md.zlib"
	tail -c +38711297 "$FILE" | head -c +3145728 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Streets of Rage 3 (USA).md.zlib"

	echo "Uncompressing ROMs ..."
	for zlib in "$ROMEXTRACT_TMPDIR/$SCRIPTID/"*.zlib; do
		printf "\\37\\213\\10\\0\\0\\0\\0\\0" | cat - "$zlib" | \
			gzip -dc > "$SCRIPTID/$(basename "${zlib%%.zlib}")" 2>/dev/null
	done

	echo "Cleaning up ..."
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
