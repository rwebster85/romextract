#!/bin/sh

# Extraction script for:
# Fight Night Round 2 (Europe) (En,Fr,De).iso

# Outputs:
# - Super Punch-Out!! (USA) (Unverified).sfc

# Requires: wit ucon64 (soft)

romextract()
{
	dependency_wit          || return 1

	echo "Extracting files from ISO ..."
	"$WIT_PATH" X "$FILE" "$ROMEXTRACT_TMPDIR/$SCRIPTID" --files=+/files/sns4q0.471

	echo "Moving files ..."
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GEYP/files/sns4q0.471" \
		"$SCRIPTID/Super Punch-Out!! (USA) (Unverified).sfc"

	if dependency_ucon64 > /dev/null 2>&1; then
		echo "Regenerating checksum for Super Punch-Out!!"
		"$UCON64_PATH" --snes --chk --nbak -o="$SCRIPTID" \
			"$SCRIPTID/Super Punch-Out!! (USA) (Unverified).sfc" > /dev/null 2>&1
	fi

	echo "Cleaning up ..."
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
