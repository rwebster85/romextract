#!/bin/sh

# Extraction script for:
# Hudson Best Collection Vol. 1 - Bomber Man Collection (Japan).gba

# Outputs:
# - Bomber Man (Japan) (Hudson Best Collection) (Unverified).nes
# - Bomber Man II (Japan) (Hudson Best Collection) (Unverified).nes

romextract()
{
	echo "Extracting ROMs ..."
	# tail for offset, head for ROM size
	tail -c +140177 "$FILE" | head -c +24592 \
		> "$SCRIPTID/Bomber Man (Japan) (Hudson Best Collection) (Unverified).nes"
	tail -c +164769 "$FILE" | head -c +131088 \
		> "$SCRIPTID/Bomber Man II (Japan) (Hudson Best Collection) (Unverified).nes"

	echo "Script $SCRIPTID.sh done"
}
